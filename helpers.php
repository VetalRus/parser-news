<?php

// function for debugging and die
function dd( $variable ) {
    echo '<pre>';
    print_r( $variable );
    echo '</pre>';
    die();
}

// function for debugging
function d( $variable ) {
    echo '<pre>';
    print_r( $variable );
    echo '</pre>';
}